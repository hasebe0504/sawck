<%@page import="dao.MessageDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

   <jsp:useBean id="userbean" scope="request"  class="beans.UserBean" />
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ルーム</title>
<link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="css/bootstrap-select.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/member.css">
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="js/member.js"></script>
</head>
<body>
	<div class="container form-container">
		<div class="row">
			<div class="col-md-12 member-form">
				<h3>ダイレクトメッセージを送る</h3>
				<form action="DirectMessageServlet" method="post">
					<div class="form-group">
						<label class="control-label">送信先:</label> <select id="users" name="users" class="form-control selectpicker" data-live-search="true" data-selected-text-format="count > 1">
							<c:forEach var="UserBean" items="${mailList}">
							<option value="${UserBean.mail}">"${UserBean.mail}"</option>
							</c:forEach>
						</select>
						<span class="users-note">追加したい人を選んでください。</span>
					</div>
					<div class="member-form-btn">
						<button class="btn btn-default" name="btn" value="cancel">キャンセル</button>
						<button id="send" class="btn btn-default" name="btn" value="create">会話を開始する</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>