<%@page import="dao.RoomDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

   <jsp:useBean id="userbean" scope="request"  class="beans.UserBean" />
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ルーム</title>
<link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="css/bootstrap-select.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/member.css">
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="js/member.js"></script>
</head>
<body>
	<div class="container form-container">
		<div class="row">
			<div class="col-md-12 member-form">
				<h3>ルームに参加する</h3>
				<form action="JoinRoomServlet?roomid=<%= request.getParameter("roomid") %>" method="post">
					<div class="form-group">
						<label class="control-label">ルーム一覧:</label> <select id="rooms" name="rooms" class="form-control selectpicker" data-live-search="true" data-selected-text-format="count > 1">
							<c:forEach var="RoomBean" items="${roomList}">
							<option value="${RoomBean.roomName}">"${RoomBean.roomName}"</option>
							</c:forEach>
						</select>
						<span class="users-note">参加したいルームを選んでください。</span>
					</div>
					<div class="member-form-btn">
						<button class="btn btn-default" name="btn" value="cancel">戻る</button>
						<button id="send" class="btn btn-default" name="btn" value="entry">参加する</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>