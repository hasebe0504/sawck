<%@page import="dao.MessageDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page  import="java.util.*" %>
    <%@page import="beans.RoomBean" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

   <jsp:useBean id="userbean" scope="request"  class="beans.UserBean" />
   <jsp:useBean id="roombean" scope="request"  class="beans.RoomBean" />
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>Swackサンプル</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
<style>
        #red {
        	background-color: red;
        }
    </style>
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/exit.js"></script>
</head>
<body>
	<div id="loading">
		<div class="loading_message">
		<script type="text/javascript">
		ran = Math.floor(Math.random()*3);

		if (ran == 0) msg = "おはようございます";
		if (ran == 1) msg = "こんにちは";
		if (ran == 2) msg = "こんばんは";

		document.write(msg);
		</script>
		</div>
		<div class="loading_message_min">－Swackのヒトコト</div>
		<div class="loading_img">
			<img src="img/loading.gif">
		</div>
	</div>
	<div id="container">
		<div class="header">
			<div class="team-menu">SJ2WAC1Aコース</div>
			<div class="room-menu">
				<span class="room-menu_name">
					<img class="room-menu_prefix" src="img/sb.png" />
					<%= request.getParameter("roomName") %>
					<!--<jsp:getProperty name="roombean" property="roomName" />-->
					<a href="StartInviteServlet?roomid=<%= request.getParameter("roomid") %>" title="メンバーを追加する">
					<img class="room-memberadd_img" src="img/ma.png" />


					<span class="membercnt">"${roombean.number}"</span></a>
				</span>


			</div>
		</div>
		<div class="main">
			<div class="listings">
				<div class="listings_rooms">
					<h2 class="listings_header">
						<a href = "StartJoinRoomServlet?userid=${userbean.userid}" title = "ルーム一覧を確認する" >ルーム</a> <a href="StartCreateRoomServlet?roomid=<%= request.getParameter("roomid") %>" title="ルームを作成する"><img class="listings_header_img" src="img/p.png" /></a>
					</h2>
					  <ul class="room_list">
						<c:forEach var="RoomBean" items="${RoomList}">
						<c:if test="${RoomBean.direct == 'off' }" >
						<c:set var="redhantei" value="${RoomBean.roomid}"/>
							<li class="room" <c:if test="${roombean.roomid ==redhantei}"><c:out value="id=\"red\""  escapeXml="false"/></c:if>><a class="room_name" href="RoomChange?roomid=${RoomBean.roomid }&roomName=${RoomBean.roomName}"><span>
							<input type="hidden" name="roomid" value="${RoomBean.roomid }">
							<input type="hidden" name="roomname" value="${RoomBean.roomName }">
							<span class="prefix">
						<c:if test="${RoomBean.publicroom =='on' }">
						<img src="img/s.png" />
						</c:if>
						<c:if test="${RoomBean.publicroom =='off' }">
						<img src="img/k.png" />
						</c:if>
							</span>
							<c:out value="${RoomBean.roomName }"/>
							</span></a></li>
						</c:if>
					</c:forEach>
				</ul>

				</div>
				<div class="listings_direct-messages">
					<h2 class="listings_header">
						<a href = "StartDirectMessageServlet?roomid=<%= request.getParameter("roomid") %>" title = "ダイレクトメッセージを開始する" >ダイレクトメッセージ</a> <img class="listings_header_img" src="img/p.png" />
					</h2>
					<ul class="room_list">
						<c:forEach var="RoomBean" items="${RoomList}">
						<c:if test="${RoomBean.direct == 'on' }">
						<c:set var="redhantei" value="${RoomBean.roomid}"/>
							<li class="room" <c:if test="${roombean.roomid ==redhantei}"><c:out value="id=\"red\""  escapeXml="false"/></c:if>><a class="room_name" href="RoomChange?roomid=${RoomBean.roomid }&roomName=${RoomBean.roomName}"><c:if test="${RoomBean.notice > 0}"><span class="unread">${RoomBean.notice }</span></c:if><span>
							<input type="hidden" name="roomid" value="${RoomBean.roomid }">
							<input type="hidden" name="roomname" value="${RoomBean.roomName }">
							<span class="prefix">
						<img src="img/s.png" />
							</span>
							<c:out value="${RoomBean.roomName }"/>
							</span></a></li>
						</c:if>
					</c:forEach>

					</ul>
				</div>
				<br>
				<br>
				<h2 class="listings_header">
						<a href = "StartComperaPasswordServlet" title = "パスワードを変更する" >パスワードの変更</a>
					</h2>
			</div>

			<div class="message-history">
				<c:forEach var="MessageBean" items="${messageList}">
				<hr>
					<div class="message">
						<span class="message_profile-pic"></span><span class="message_username">
						<c:out value="${MessageBean.name}"/></span>
						<span class="message_timestamp"><c:out value="${MessageBean.dates }"/></span>
						<span class="message_star"></span>
						<span class="message_content"> <c:out value="${MessageBean.text }"/> </span>
					</div>
					<c:set var="jouken" value="${MessageBean.name}"/>
					<c:if test="${userbean.name == jouken}">
					<form action="DeleteMessageServlet?id=${MessageBean.messageid}&roomid=${roombean.roomid }" method="post" id="DeleteMessageServlet">
						<input  type="submit" id="send" value="削除" />
						</form>
						</c:if>

				</c:forEach>
				</div>
			</div>
		</div>
		<div class="footer">
			<div class="user-menu">
				<a href="Logout" title="Swackからサインアウトする"> <span class="user-menu_profile-pic"></span> <span class="user-menu_username"><jsp:getProperty name="userbean" property="name" /></span> <span class="connection_status">ログイン中</span>
				</a>
			</div>
			<form action="ChatEntry?roomid=${roombean.roomid }&roomName=${roombean.roomName }" method="post" id="sendMessage">
			<input type="hidden" name="roomid" value=<jsp:getProperty name="roombean" property="roomid" />>
				<div class="input-box">
					<input class="input-box_text" type="text" id="message" name="message" placeholder="#everyoneへのメッセージ" autocomplete="off" />
					<input class="input-box_button" type="submit" id="send" value="送信" />
				</div>
			</form>
		</div>
</body>
</html>