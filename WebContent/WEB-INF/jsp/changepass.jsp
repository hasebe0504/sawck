<%@page import="dao.UserDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>パスワードの変更</title>
</head>
<body>
<div class="container form-container">
		<div class="row">
			<div class="col-md-6 login-form">
				<h3>パスワードの変更</h3>
				<h5>新しいパスワードを入力してください。</h5>
				<form action="ComperaPasswordServlet" method="post">
					<div class="form-group">
						<input type="password"  name="nowpassword" id="checkPassword" class="form-control" placeholder="現在パスワード" required  />
					</div>
					<div class="form-group">
						<input type="password" name="newpassword" id="newPassword" class="form-control" placeholder="新規パスワード" required />
					</div>
					<div class="form-group">
						<input type="submit" class="btnSubmit" value="パスワードの変更" />
					</div>
					</form>
					</div>
					</div>
					</div>
</body>
</html>