package beans;

public class RoomBean {
	private String roomid;
	private String roomName;
	private String publicroom;
	private String direct;
	private int notice;
	public int Number;

	public String getRoomid() {
		return roomid;
	}

	public void setRoomid(String roomid) {
		this.roomid = roomid;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getPublicroom() {
		return publicroom;
	}

	public void setPublicroom(String publicroom) {
		this.publicroom = publicroom;
	}

	public String getDirect() {
		return direct;
	}

	public void setDirect(String direct) {
		this.direct = direct;
	}

	public int getNumber() {
		return Number;
	}

	public void setNumber(int number) {
		Number = number;
	}

	public int getNotice() {
		return notice;
	}

	public void setNotice(int notice) {
		this.notice = notice;
	}
}
