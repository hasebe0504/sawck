package dao;

import static parameter.DAOParameters.*;
import static parameter.ExceptionParameters.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import exception.DatabaseException;

public class MaxTextIDDAO {
	int i = 0;

	public int MaxID() throws DatabaseException {
		String sql = "SELECT max(messageid) as max FROM message";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst = conn.prepareStatement(sql);) {
				ResultSet rs = pst.executeQuery();
				while (rs.next()) {
					i = rs.getInt("max") + 1;
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return i;
	}
}
