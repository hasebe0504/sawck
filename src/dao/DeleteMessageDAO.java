package dao;

import static parameter.DAOParameters.*;
import static parameter.ExceptionParameters.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import exception.DatabaseException;

public class DeleteMessageDAO {

	public void Delete(String messageid) throws DatabaseException {
		String sql = "DELETE FROM message WHERE messageid=?";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setString(1, messageid);
				int rs = pst.executeUpdate();

			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
	}
}
