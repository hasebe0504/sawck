package dao;

import static parameter.DAOParameters.*;
import static parameter.ExceptionParameters.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import exception.DatabaseException;

@WebServlet("/CipherDAO")
public class CipherDAO extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public static void reference(String userid, String pass) throws DatabaseException {
		ResultSet rs = null;
		String sql = "UPDATE SUSER SET password = " + pass + "WHERE userid = +" + userid + "";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst = conn.prepareStatement(sql);) {
				rs = pst.executeQuery();
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
	}
}