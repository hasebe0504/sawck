package dao;

import static parameter.DAOParameters.*;
import static parameter.ExceptionParameters.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import exception.DatabaseException;

public class NoticeDAO {

	public int Notice(String roomid, String Userid) throws DatabaseException {
		int notice = 0;
		String sql = "SELECT COUNT(*) AS count FROM message M,participant P WHERE M.days > P.lastlogin and M.userid != ? and M.roomid = ? and m.userid = p.USERID and m.ROOMID = p.ROOMID";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setString(1, Userid);
				pst.setString(2, roomid);
				ResultSet rs = pst.executeQuery();
				while (rs.next()) {
					notice = rs.getInt("count");
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}

		return notice;
	}
}
