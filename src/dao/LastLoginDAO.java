package dao;

import static parameter.DAOParameters.*;
import static parameter.ExceptionParameters.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import exception.DatabaseException;

public class LastLoginDAO {
	public void LastLogin(String userid, String roomID) throws DatabaseException {
		try {
			Class.forName(DRIVER_NAME);
			String sql = "UPDATE PATICIPANT SET lastlogin = syadate WHERE userid = ? AND roomid = ?";
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst = conn.prepareStatement(sql);) {

				pst.setString(1, userid);
				pst.setString(2, roomID);
				pst.executeUpdate();

			}
		} catch (SQLException | ClassNotFoundException e) {

			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
	}
}
