package dao;

import static parameter.DAOParameters.*;
import static parameter.ExceptionParameters.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import exception.DatabaseException;

public class InviteDAO {
	public void Invite(String ROOMID, String USER) throws DatabaseException {
		String sql = "INSERT INTO PARTICIPANT VALUES(?,?,sysdate)";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setString(1, USER);
				pst.setString(2, ROOMID);
				pst.executeQuery();

			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
	}
}
