package dao;

import static parameter.DAOParameters.*;
import static parameter.ExceptionParameters.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.RoomBean;
import beans.UserBean;
import exception.DatabaseException;

public class SerchRoomDAO {

	public SerchRoomDAO() {

	}

	public ArrayList<RoomBean> serchRoom(UserBean userBean) throws DatabaseException {
		ArrayList<RoomBean> roomList = new ArrayList<RoomBean>();
		NumberDAO numberdao = new NumberDAO();
		RoomBean roombeans = new RoomBean();
		NoticeDAO noticedao = new NoticeDAO();
		roombeans.setRoomid("1");
		roombeans.setRoomName("everyone");
		roombeans.setDirect("off");
		roombeans.setPublicroom("on");

		roombeans.setNotice(noticedao.Notice("1", userBean.getUserid()));
		roomList.add(roombeans);
		String sql = "SELECT R.roomid,R.roomname, R.direct,R.publicroom from room R,suser S,participant P WHERE R.roomid = P.roomID AND S.userid = P.userid AND S.userid = ?";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setString(1, userBean.getUserid());
				ResultSet rs = pst.executeQuery();

				while (rs.next()) {
					RoomBean roombean = new RoomBean();
					roombean.setRoomid(rs.getString("roomid"));
					if (rs.getString("direct").compareTo("on") == 0) {
						String sql2 = "SELECT S.name  FROM participant P,suser S WHERE P.roomid=? AND P.userid =S.userid AND P.userid != ?";
						System.out.println(rs.getString("roomid"));
						try {
							Class.forName(DRIVER_NAME);
							try (Connection cone = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst2 = cone.prepareStatement(sql2);) {
								pst2.setString(1, rs.getString("roomid"));
								pst2.setString(2, userBean.getUserid());
								ResultSet rs2 = pst2.executeQuery();
								String userroom = null;
								System.out.println("userid:" + userBean.getUserid());
								while (rs2.next()) {
									if (userroom != null) {
										userroom = userroom + rs2.getString("NAME") + " ";
									} else {
										userroom = rs2.getString("name") + " ";
									}
								}
								roombean.setRoomName(userroom);
							}

						} catch (SQLException | ClassNotFoundException e) {
							throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
						}
					} else {
						roombean.setRoomName(rs.getString("roomname"));
					}
					roombean.setNumber(numberdao.NumberCnt(rs.getString("roomid")));
					roombean.setDirect(rs.getString("DIRECT"));
					roombean.setPublicroom(rs.getString("publicroom"));
					roombeans.setNotice(noticedao.Notice(rs.getString("roomid"), userBean.getUserid()));
					roomList.add(roombean);
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}

		return roomList;
	}
}
