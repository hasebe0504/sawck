package dao;

import static parameter.DAOParameters.*;
import static parameter.ExceptionParameters.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import exception.DatabaseException;

public class NumberDAO {
	int Number;

	public int NumberCnt(String roomid) throws DatabaseException {
		String sql = "SELECT COUNT(userid) AS count FROM PARTICIPANT WHERE roomid = ?";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setString(1, roomid);
				ResultSet rs = pst.executeQuery();
				while (rs.next()) {
					Number = rs.getInt("count");
				}
			}
			return Number;
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
	}

}
