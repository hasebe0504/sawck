package dao;

import static parameter.DAOParameters.*;
import static parameter.ExceptionParameters.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.MessageBean;
import exception.DatabaseException;

public class MessageDAO {

	public ArrayList<MessageBean> selectText(ArrayList<String> id, String roomID) throws DatabaseException {
		ArrayList<MessageBean> messageList = new ArrayList<>();
		ResultSet rs = null;
		for (String userid : id) {
			String sql = "SELECT M.userid,M.days,M.text,S.name,M.messageID FROM message M,suser S WHERE M.userid = ? and M.roomid = ? and M.userid = S.userid";
			try {
				Class.forName(DRIVER_NAME);
				try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst = conn.prepareStatement(sql);) {

					pst.setString(1, userid);
					pst.setString(2, roomID);
					rs = pst.executeQuery();
					while (rs.next()) {
						MessageBean messageBean = new MessageBean();
						messageBean.setUserid(rs.getInt("userid"));
						messageBean.setDates(rs.getDate("days"));
						messageBean.setText(rs.getString("text"));
						messageBean.setName(rs.getString("name"));
						messageBean.setMessageid(rs.getInt("messageid"));
						messageList.add(messageBean);
					}
				}
			} catch (SQLException | ClassNotFoundException e) {

				throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
			}
		}

		return messageList;
	}

}
