package dao;

import static parameter.DAOParameters.*;
import static parameter.ExceptionParameters.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.UserBean;
import exception.DatabaseException;

public class UserDAO {

	public ArrayList<UserBean> selectScoreAverageList() throws DatabaseException {
		ArrayList<UserBean> userList = new ArrayList<>();

		String sql = "SELECT subjectname, avg(score) FROM grade g JOIN subject s ON g.subjectid = s.subjectid GROUP BY subjectname";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					UserBean userBean = new UserBean();
					userBean.setName(rs.getString("name"));
					userBean.setMail(rs.getString("email"));
					userBean.setPassword(rs.getString("password"));
					userList.add(userBean);
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);

		}
		return userList;
	}

	public int register(UserBean userBean) throws DatabaseException {
		int cnt = 0;
		String sql = "INSERT INTO suser VALUES(?, ?, ?, ?)";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setString(1, userBean.getUserid());
				pst.setString(2, userBean.getName());
				pst.setString(3, userBean.getMail());
				pst.setString(4, userBean.getPassword());
				cnt = pst.executeUpdate();
			}
			System.out.println("INSERT INTO suser" + cnt + "件");
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return cnt;
	}

	public UserBean Login(UserBean userBean) throws DatabaseException {
		UserBean users = userBean;
		ResultSet rs = null;
		int cnt = 0;
		String sql = "SELECT * FROM suser WHERE mail = ? and password =?";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setString(1, users.getMail());
				pst.setString(2, users.getPassword());

				rs = pst.executeQuery();
				while (rs.next()) {
					userBean.setName(rs.getString("name"));
					userBean.setUserid(rs.getString("userid"));
					cnt++;
				}
			}
			System.out.println("該当ユーザ" + cnt + "件");
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}

		return userBean;
	}

	public int getID(String userid) throws DatabaseException {
		int id = 0;
		ResultSet rs = null;
		int cnt = 0;
		String sql = "SELECT * FROM suser WHERE mail = ? and password =?";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setString(1, userid);

				rs = pst.executeQuery();
				while (rs.next()) {
					id = rs.getInt("userid");
					cnt++;
				}
			}
			System.out.println("該当ユーザ" + cnt + "件");
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}

		return id;
	}

	public String getNextID(String userid) throws DatabaseException {
		int id = 0;
		ResultSet rs = null;
		int cnt = 0;
		String sql = "SELECT MAX(USERID) AS USERID FROM suser";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst = conn.prepareStatement(sql);) {
				rs = pst.executeQuery();
				while (rs.next()) {
					id = rs.getInt("USERID") + 1;
					cnt++;
				}
			}
			System.out.println("該当ユーザ" + cnt + "件");
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}

		return String.valueOf(id);
	}

	public UserBean getUserSession(UserBean userbean) throws DatabaseException {
		UserBean loginsession = new UserBean();
		ResultSet rs = null;
		int cnt = 0;
		String sql = "SELECT * FROM suser WHERE mail = ? and password =?";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setString(1, userbean.getMail());
				pst.setString(2, userbean.getPassword());

				rs = pst.executeQuery();
				while (rs.next()) {
					loginsession.setName(rs.getString("name"));
					loginsession.setMail(rs.getString("mail"));
					loginsession.setUserid(rs.getString("userid"));
					cnt++;
				}
			}
			System.out.println("該当ユーザ" + cnt + "件");
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}

		return loginsession;
	}

	public ArrayList<UserBean> selectUser(String workspaceId) throws DatabaseException {
		ResultSet rs = null;
		ArrayList<UserBean> userList = new ArrayList<UserBean>();
		String sql = "SELECT DISTINCT S.userid,S.name,S.mail FROM message M,suser S WHERE M.roomid = ? AND S.userid = M.userid";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst = conn.prepareStatement(sql);) {

				pst.setString(1, workspaceId);
				rs = pst.executeQuery();
				while (rs.next()) {
					UserBean userbean = new UserBean();
					userbean.setUserid(rs.getString("userid"));
					userbean.setName(rs.getString("name"));
					userbean.setMail(rs.getString("mail"));
					userList.add(userbean);
				}
			}
		} catch (SQLException | ClassNotFoundException e) {

			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}

		return userList;
	}

	public ArrayList<UserBean> selectMail(String roomid) throws DatabaseException {
		ResultSet rs = null;
		ArrayList<UserBean> userList = new ArrayList<UserBean>();
		String sql = "SELECT MAIL FROM SUSER S WHERE  NOT EXISTS(SELECT * FROM PARTICIPANT P WHERE S.USERID = P.USERID AND P.ROOMID = ?)";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setString(1, roomid);
				rs = pst.executeQuery();
				while (rs.next()) {
					UserBean userbean = new UserBean();
					userbean.setMail(rs.getString("mail"));
					userList.add(userbean);
				}
			}
		} catch (SQLException | ClassNotFoundException e) {

			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}

		return userList;
	}

	public ArrayList<UserBean> selectAllMail() throws DatabaseException {
		ResultSet rs = null;
		ArrayList<UserBean> userList = new ArrayList<UserBean>();
		String sql = "SELECT MAIL FROM SUSER"; //ここから重複消すよ
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst = conn.prepareStatement(sql);) {
				rs = pst.executeQuery();
				while (rs.next()) {
					UserBean userbean = new UserBean();
					userbean.setMail(rs.getString("mail"));
					userList.add(userbean);
				}
			}
		} catch (SQLException | ClassNotFoundException e) {

			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}

		return userList;
	}

	public String MailToUserid(String mail) throws DatabaseException {
		ResultSet rs = null;
		String userid;
		String sql = "SELECT userid FROM SUSER WHERE mail = ?";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setString(1, mail);
				rs = pst.executeQuery();
				rs.next();
				userid = rs.getString(1);
			}
		} catch (SQLException | ClassNotFoundException e) {

			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return userid;
	}

	public String UseridToName(String userid) throws DatabaseException {
		ResultSet rs = null;
		String username;
		String sql = "SELECT name FROM SUSER WHERE userid = ?";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setString(1, userid);
				rs = pst.executeQuery();
				rs.next();
				username = rs.getString(1);
			}
		} catch (SQLException | ClassNotFoundException e) {

			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return username;
	}
}
