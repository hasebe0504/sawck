package dao;

import static parameter.DAOParameters.*;
import static parameter.ExceptionParameters.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.ChatBean;
import exception.DatabaseException;

public class ChatDAO {
	public ArrayList<ChatBean> selectAll() throws DatabaseException {
		ArrayList<ChatBean> chatBeanList = new ArrayList<ChatBean>();
		String sql = "SELECT * FROM message ORDER BY postdate ASC";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					ChatBean chatBean = new ChatBean();
					chatBean.setUserid(rs.getString("userid"));
					chatBean.setText(rs.getString("text"));
					chatBean.setRoomid(rs.getString("roomid"));
					chatBean.setChatid(rs.getString("messageid"));
					chatBeanList.add(chatBean);
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return chatBeanList;
	}

	public int register(ChatBean chatBean) throws DatabaseException {
		int cnt = 0;
		MaxTextIDDAO maxid = new MaxTextIDDAO();
		int i = maxid.MaxID();
		String sql = "INSERT INTO message VALUES(?, sysdate, ? , ?," + i + ")";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD);
					PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setString(1, chatBean.getUserid());
				pst.setString(2, chatBean.getText());
				pst.setString(3, chatBean.getRoomid());
				cnt = pst.executeUpdate();
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return cnt;
	}
}