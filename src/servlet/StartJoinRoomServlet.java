package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.RoomBean;
import dao.RoomDAO;
import exception.DatabaseException;

/**
 * Servlet implementation class StartJoinRoomServlet
 */
@WebServlet("/StartJoinRoomServlet")
public class StartJoinRoomServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String userid = request.getParameter("userid");
		RoomDAO roomDAO = new RoomDAO();
		ArrayList<RoomBean> roomList = new ArrayList<RoomBean>();
		try {
			roomList = roomDAO.entryRoom(userid);
		} catch (DatabaseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		session.setAttribute("roomList", roomList);
		getServletContext().getRequestDispatcher("/WEB-INF/jsp/roomlist.jsp").forward(request, response);
	}

}
