package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBean;
import dao.InviteDAO;
import dao.RoomDAO;
import dao.UserDAO;
import exception.DatabaseException;

/**
 * Servlet implementation class DirectMessageServlet
 */
@WebServlet("/DirectMessageServlet")
public class DirectMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DirectMessageServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RoomDAO roomDAO = new RoomDAO();
		InviteDAO inviteDAO = new InviteDAO();
		UserDAO userDAO = new UserDAO();
		HttpSession session = request.getSession();

		String roomid = null;
		String userid = null;
		String usermail = null;

		UserBean userbean = new UserBean();
		userbean = (UserBean) session.getAttribute("loginsession");

		if (request.getParameter("btn").equals("create")) {
			try {
				usermail = request.getParameter("users");
				userid = userDAO.MailToUserid(usermail);
				roomid = roomDAO.createRoom(userDAO.UseridToName(userid), userbean, "off", "on");
				if (userbean.getUserid() != userid) {
					inviteDAO.Invite(roomid, userDAO.MailToUserid(usermail));
				}
			} catch (DatabaseException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
		} else {
			roomid = "1";
		}

		try {
			ArrayList<UserBean> beanList = new ArrayList<UserBean>();
			beanList = userDAO.selectUser(roomid);
			request.setAttribute("userbean", userbean);
			request.setAttribute("beanList", beanList);
			getServletContext().getRequestDispatcher("/RoomChange?roomid=" + roomid).forward(request, response);
		} catch (DatabaseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

}