package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ChatBean;
import beans.RoomBean;
import beans.UserBean;
import dao.ChatDAO;
import dao.UserDAO;
import exception.DatabaseException;

@WebServlet("/ChatEntry")
public class ChatEntry extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* HttpSession session = request.getSession(); LoginSession loginSession = (LoginSession) session.getAttribute("loginSession"); */
		HttpSession session = request.getSession();
		UserDAO userdao = new UserDAO();
		ChatBean chatBean = new ChatBean();
		RoomBean roomBean = new RoomBean();
		UserBean userbean = (UserBean) session.getAttribute("loginsession");
		String id = userbean.getUserid();
		chatBean.setUserid(id);
		chatBean.setText(request.getParameter("message"));
		chatBean.setRoomid(request.getParameter("roomid"));
		roomBean.setRoomName(request.getParameter("roomName"));
		roomBean.setRoomid(request.getParameter("roomid"));
		ChatDAO chatDAO = new ChatDAO();
		request.setAttribute("userbean", userbean);
		try {
			ArrayList<UserBean> beanList = userdao.selectUser(chatBean.getRoomid());
			request.setAttribute("beanList", beanList);
		} catch (DatabaseException e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}
		try {
			chatDAO.register(chatBean);
		} catch (DatabaseException e) {
			e.printStackTrace();
		}

		getServletContext().getRequestDispatcher("/RoomChange?roomid=" + chatBean.getRoomid()).forward(request, response);
		//response.sendRedirect("login.html");
		// getServletContext().getRequestDispatcher("/Index").forward(request, response);
	}
}