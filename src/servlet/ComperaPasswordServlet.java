package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBean;
import dao.CipherDAO;
import dao.UserDAO;
import exception.DatabaseException;

@WebServlet("/ComperaPasswordServlet")
public class ComperaPasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserDAO userDAO = new UserDAO();
		UserBean userbean = (UserBean) session.getAttribute("loginsession");
		String userid = (String) userbean.getUserid();
		String password = (String) userbean.getPassword();
		String nowpassword = request.getParameter("nowpassword");
		String pass = request.getParameter("newpassword");
		if (password.equals(nowpassword)) {
			try {
				//String newpass = (request.getParameter("newpassword"));
				CipherDAO.reference(userid, pass);
			} catch (DatabaseException e) {
				this.error(request, response);
				return;
			}
		} else {
			this.error(request, response);
			return;
		}
		//userbean.setPassword(request.getParameter("password"));
		ArrayList<UserBean> beanList = new ArrayList<UserBean>();
		try {
			beanList = userDAO.selectUser("1");
			request.setAttribute("userbean", userbean);
			request.setAttribute("beanList", beanList);
			getServletContext().getRequestDispatcher("/RoomChange?roomid=" + "1").forward(request, response);
		} catch (DatabaseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

	public void error(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//getServletContext().getRequestDispatcher("/WEB-INF/jsp/error.html").forward(request, response);
		response.sendRedirect("error.html");
	}

}
