package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.MessageBean;
import beans.RoomBean;
import beans.UserBean;
import dao.MessageDAO;
import dao.NumberDAO;
import dao.SerchRoomDAO;
import exception.DatabaseException;

@WebServlet("/TextServlet")
public class TextServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<MessageBean> messageList = new ArrayList<MessageBean>();
		HttpSession session = request.getSession();
		UserBean userbean = (UserBean) session.getAttribute("loginsession");
		MessageDAO messagedao = new MessageDAO();
		SerchRoomDAO roomdao = new SerchRoomDAO();
		ArrayList<String> idList = new ArrayList<String>();
		ArrayList<UserBean> userList = (ArrayList<UserBean>) request.getAttribute("beanList");
		RoomBean roombean = new RoomBean();
		NumberDAO numberdao = new NumberDAO();

		if ((RoomBean) request.getAttribute("roombean") == null) { //遷移先のルームが指定されていない場合、everyoneに飛ぶ
			roombean.setRoomid("1");
			roombean.setRoomName("everyone");
		} else {
			roombean = (RoomBean) request.getAttribute("roombean");
		}

		try {
			roombean.setNumber(numberdao.NumberCnt(roombean.getRoomid()));
		} catch (DatabaseException e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}
		if (roombean.getRoomid() == "1") {
			roombean.setNumber(userList.size());
		}
		request.setAttribute("roombean", roombean);
		session.setAttribute("roomid", roombean.getRoomid());

		try {
			for (UserBean userbeans : userList) {
				String id = userbeans.getUserid();
				idList.add(id);
				messageList = messagedao.selectText(idList, roombean.getRoomid()); //遷移先のページのメッセージを抽出
			}

			ArrayList<RoomBean> roomList = roomdao.serchRoom(userbean);//ログイン中のユーザが参加中のルーム一覧wowow
			session.setAttribute("RoomList", roomList);

		} catch (DatabaseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		for (int i = 0; i < messageList.size(); i++) { //messageListの並び替え処理
			int z = i;
			MessageBean messagebean = messageList.get(i);
			for (int j = i + 1; j < messageList.size(); j++) {
				MessageBean hikakubean = messageList.get(j);
				if (messageList.get(z).getDates().compareTo(hikakubean.getDates()) == 1) {
					z = j;
				}
			}
			MessageBean karioki = messagebean;
			messageList.set(i, messageList.get(z));
			messageList.set(z, karioki);
		}

		session.setAttribute("messageList", messageList);

		getServletContext().getRequestDispatcher("/WEB-INF/jsp/index.jsp").forward(request, response);
	}

}
