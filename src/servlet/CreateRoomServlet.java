package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBean;
import dao.InviteDAO;
import dao.RoomDAO;
import dao.UserDAO;
import exception.DatabaseException;

/**
 * Servlet implementation class CreateRoom
 */
@WebServlet("/CreateRoomServlet")
public class CreateRoomServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreateRoomServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RoomDAO roomDAO = new RoomDAO();
		InviteDAO invitedao = new InviteDAO();
		UserDAO userdao = new UserDAO();
		UserBean userbean = new UserBean();
		HttpSession session = request.getSession();
		userbean = (UserBean) session.getAttribute("loginsession");
		String[] usermail = request.getParameterValues("users");
		String roomid = "";
		String check = request.getParameter("check");
		if (check == null) {
			check = "off";
		}

		try {
			if (request.getParameter("btn").equals("create")) {
				roomid = roomDAO.createRoom(request.getParameter("name"), userbean, check, "off");
				System.out.print(roomid);
				if (usermail[0] == null) {
				} else {
					for (int a = 0; usermail.length > a; a++) {
						System.out.println(usermail[a]);
						invitedao.Invite(roomid, userdao.MailToUserid(usermail[a]));
					}
				}
			}

			ArrayList<UserBean> beanList = new ArrayList<UserBean>();
			beanList = userdao.selectUser(roomid);
			request.setAttribute("roomid", roomid);
			request.setAttribute("userbean", userbean);
			request.setAttribute("beanList", beanList);
			getServletContext().getRequestDispatcher("/RoomChange?roomid=" + roomid).forward(request, response);
		} catch (DatabaseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

}
