package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBean;
import dao.InviteDAO;
import dao.UserDAO;
import exception.DatabaseException;

@WebServlet("/ExitServlet")
public class ExitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ExitServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		InviteDAO invitedao = new InviteDAO();
		UserDAO userdao = new UserDAO();
		UserBean userbean = new UserBean();
		HttpSession session = request.getSession();
		userbean = (UserBean) session.getAttribute("loginsession");

		String roomid = request.getParameter("roomid");
		String[] usermail = request.getParameterValues("users");
		try {
			if (request.getParameter("btn").equals("invite")) {
				for (int a = 0; usermail.length > a; a++) {
					invitedao.Invite(roomid, userdao.MailToUserid(usermail[a]));
				}
			}
			ArrayList<UserBean> beanList = new ArrayList<UserBean>();
			beanList = userdao.selectUser("1");
			request.setAttribute("userbean", userbean);
			request.setAttribute("beanList", beanList);
			getServletContext().getRequestDispatcher("/TextServlet").forward(request, response);
		} catch (DatabaseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

}
