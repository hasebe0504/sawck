package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBean;
import dao.UserDAO;
import exception.DatabaseException;

/**
 * 00000 Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private String name;
	private String mail;
	private String password;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<UserBean> beanList = new ArrayList<UserBean>(); //特定のルームに参加してるユーザのユーザービンのリスト
		UserBean userbean = new UserBean();//ログイン中のユーザのセット（ログインサーブレットだからであり、他ではセッションから取り出してくれ）
		userbean.setMail(request.getParameter("email"));
		userbean.setPassword(request.getParameter("password"));//ログインhtmlから情報を持ってきて、ベーンに入れてる

		UserDAO userdao = new UserDAO();
		try {
			userbean = userdao.Login(userbean);//ログイン特有の処理、他ではセッションから取り出す　userbean = session.getAttribute("loginsession");
			if (userbean.getName() == null) {
				getServletContext().getRequestDispatcher("/login.html").forward(request, response);
				return;
			}
			beanList = userdao.selectUser("1"); //roomid=1に参加しているユーザのusebeanのリストを作成する
			request.setAttribute("userbean", userbean); //ログイン中のユーザのユーザビーン
			request.setAttribute("beanList", beanList);//テキストサーブレットに参加中のユーザーのユーザービンをまとめて送ってる
			//request.setAttribute("userid", userbean.getUserid());//たぶんいらない
			HttpSession session = request.getSession();
			UserBean loginsession = userbean;
			session.setAttribute("loginsession", loginsession);
		} catch (DatabaseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		getServletContext().getRequestDispatcher("/TextServlet?roomName=everyone").forward(request, response);

	}

}
