package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBean;
import dao.UserDAO;
import exception.DatabaseException;

/**
 * Servlet implementation class InviteServlet
 */
@WebServlet("/StartInviteServlet")
public class StartInviteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public StartInviteServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserDAO userdao = new UserDAO();
		ArrayList<UserBean> mailList = new ArrayList<UserBean>();

		try {
			mailList = userdao.selectMail(request.getParameter("roomid"));
		} catch (DatabaseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		session.setAttribute("mailList", mailList);
		getServletContext().getRequestDispatcher("/WEB-INF/jsp/invite.jsp").forward(request, response);
	}
}