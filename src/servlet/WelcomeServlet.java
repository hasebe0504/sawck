package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBean;
import dao.UserDAO;
import exception.DatabaseException;

/**
 * Servlet implementation class WelcomeServlet
 */
@WebServlet("/WelcomeServlet")
public class WelcomeServlet extends HttpServlet {
	UserDAO userdao = new UserDAO();
	ArrayList<UserBean> beanList = new ArrayList<UserBean>();

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<UserBean> beanList = new ArrayList<UserBean>();
		HttpSession session = request.getSession(false);
		if (session == null) { //セッションあるかチェック
			response.sendRedirect("login.html");
			return;
		} else {
			//session = request.getSession();
			UserBean userbean = (UserBean) session.getAttribute("loginsession");
			if (userbean == null) {
				response.sendRedirect("login.html");
				return;
			}
			try {
				beanList = userdao.selectUser("1");
			} catch (DatabaseException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			request.setAttribute("userbean", userbean);
			request.setAttribute("beanList", beanList);

			getServletContext().getRequestDispatcher("/RoomChange?roomid=" + session.getAttribute("roomid")).forward(request, response);
		}
	}

}
