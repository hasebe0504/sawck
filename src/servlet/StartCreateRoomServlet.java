package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBean;
import dao.UserDAO;
import exception.DatabaseException;

/**
 * Servlet implementation class StartCreateRoomServlet
 */
@WebServlet("/StartCreateRoomServlet")
public class StartCreateRoomServlet extends HttpServlet {

	public StartCreateRoomServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserDAO userdao = new UserDAO();
		ArrayList<UserBean> mailList = new ArrayList<UserBean>();

		try {
			mailList = userdao.selectAllMail();
		} catch (DatabaseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		session.setAttribute("mailList", mailList);
		getServletContext().getRequestDispatcher("/WEB-INF/jsp/room.jsp").forward(request, response);
	}

}
