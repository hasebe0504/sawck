package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.RoomBean;
import beans.UserBean;
import dao.InviteDAO;
import dao.RoomDAO;
import dao.UserDAO;
import exception.DatabaseException;

@WebServlet("/JoinRoomServlet")
public class JoinRoomServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		InviteDAO inviteDAO = new InviteDAO();
		RoomDAO roomDAO = new RoomDAO();
		UserDAO userDAO = new UserDAO();
		HttpSession session = request.getSession();
		UserBean userbean = (UserBean) session.getAttribute("loginsession");
		String roomName = request.getParameter("rooms");
		String roomid = null;

		if (request.getParameter("btn").equals("entry")) {
			try {
				roomid = roomDAO.RoomNameToID(roomName);
				inviteDAO.Invite(roomid, userbean.getUserid());
			} catch (DatabaseException e) {
				e.printStackTrace();
			}
		}
		ArrayList<RoomBean> roomList = new ArrayList<RoomBean>();
		try {
			roomList = roomDAO.entryRoom("1");
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
		ArrayList<UserBean> beanList = new ArrayList<UserBean>();
		try {
			beanList = userDAO.selectUser(roomid);
			request.setAttribute("userbean", userbean);
			request.setAttribute("beanList", beanList);
			//session.setAttribute("roomList", roomList);
			getServletContext().getRequestDispatcher("/RoomChange?roomid=" + roomid).forward(request, response);
		} catch (DatabaseException e) {
			e.printStackTrace();
		}

	}
}