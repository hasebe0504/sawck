package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserBean;
import dao.InviteDAO;
import dao.UserDAO;
import exception.DatabaseException;

@WebServlet("/JoinWorkSpaceServlet")
public class JoinWorkSpaceServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserBean userbean = new UserBean();
		UserDAO userDAO = new UserDAO();
		InviteDAO inviteDAO = new InviteDAO();
		String id = null;
		try {
			id = userDAO.getNextID("userid");
		} catch (DatabaseException e1) {
			e1.printStackTrace();
		}
		userbean.setUserid(id);
		userbean.setName(request.getParameter("name"));
		userbean.setMail(request.getParameter("email"));
		userbean.setPassword(request.getParameter("password"));

		try {
			userDAO.register(userbean);
			inviteDAO.Invite("1", id);

		} catch (DatabaseException e) {
			System.out.println("catch!登録失敗");
			e.printStackTrace();
		}

		getServletContext().getRequestDispatcher("/login.html").forward(request, response);
	}

}