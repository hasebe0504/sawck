package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.RoomBean;
import beans.UserBean;
import dao.RoomDAO;
import dao.UserDAO;
import exception.DatabaseException;

@WebServlet("/RoomChange")
public class RoomChange extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserBean userbean = (UserBean) session.getAttribute("loginsession");
		UserDAO userdao = new UserDAO();
		RoomDAO roomdao = new RoomDAO();

		try {
			String roomid = request.getParameter("roomid");
			RoomBean roomBean = new RoomBean();
			roomBean.setRoomid(roomid);
			roomBean.setRoomName(roomdao.getRoomName(roomid));
			ArrayList<UserBean> beanList = userdao.selectUser(roomid); //
			request.setAttribute("roombean", roomBean);
			request.setAttribute("userbean", userbean); //ログイン中のユーザのユーザビーン
			request.setAttribute("beanList", beanList);//テキストサーブレットに参加中のユーザーのユーザービンをまとめて送ってる
			getServletContext().getRequestDispatcher("/TextServlet?roomName=" + roomBean.getRoomName()).forward(request, response);
		} catch (DatabaseException e) {
			e.printStackTrace();
		} //roomid=1に参加しているユーザのusebeanのリストを作成する
	}

}
