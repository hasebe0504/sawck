package parameter;

public class DAOParameters {
	public static final String DRIVER_NAME = "oracle.jdbc.driver.OracleDriver";
	public static final String CONNECT_STRING = "jdbc:oracle:thin:@10.11.39.215:1521:HCS1";
	public static final String USERID = "h20183053";
	public static final String PASSWORD = "oraclemaster";
	//	public static final String CONNECT_STRING = "jdbc:oracle:thin:@localhost:1521:xe";
	//	public static final String USERID = "local";
	//	public static final String PASSWORD = "1234";
}
